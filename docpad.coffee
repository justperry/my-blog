# Depdendencies
moment = require('moment')

# Main config
docpadConfig =

    # Collections
    collections:
        pages: ->
            @getCollection('html')
                .findAllLive({relativeOutDirPath: 'pages'},[{ date: -1 }])
                .on 'add', (model) ->
                    model.setMetaDefaults({ layout:'page' })
        posts: ->
            @getCollection('html')
                .findAllLive({relativeOutDirPath: 'posts'},[{ date: -1 }])
                .on 'add', (model) ->
                    model.setMetaDefaults({ layout:'post' })
        projects: ->
            @getCollection('html')
                .findAllLive({relativeOutDirPath: 'projects'},[{ date: -1 }])
                .on 'add', (model) ->
                    model.setMetaDefaults({ layout:'project' })
        photos: ->
            @getCollection('html')
                .findAllLive({relativeOutDirPath: 'photos'},[{ date: -1 }])
                .on 'add', (model) ->
                    model.setMetaDefaults({ layout:'photo' })


    # Template variables and abstractions
    templateData:

        # Site-wide variables
        site:
            url: 'http://www.bionico.co.uk'
            title: 'Justin Perry'
            description: 'Musings from Justin Perry'
            strapline: 'Front-end stuff'
            keywords: 'javascript, requirejs, responsive, rwd, front-end'
            services: 
                disqus: 'frontender'

        # Abstractions

        # Show current year
        getYear: ->
            date = new Date()
            date.getFullYear()

        # Show custom page title. Or return default site title.
        getPreparedTitle: ->
            if @document.title
                '' + @document.title
            else
                @site.title

        # Show custom page description. Or return default site description.
        getPreparedDescription: ->
            @document.description or @site.description

        # Show custom keywords. Or return default site keywords.
        getPreparedKeywords: ->
            @site.keywords.concat(@document.keywords or []).join ', '


        # Post meta
        # Creds: https://github.com/Greduan/eduantech.docpad/blob/bcc91a247e9741f4ce8aa5883634fac26c9859a5/docpad.coffee
        postDatetime: (date, format="YYYY-MM-DD") -> 
            moment(date).format(format)
        postDate: (date, format="MMMM DD, YYYY") -> 
            moment(date).format(format)

    # Environments
    environments:
        development:
            outPath: 'out'

        production:
            outPath: 'build'

    # Plugin configuration
    plugins:
        sass:
            renderUnderscoreStylesheets: false
            debugInfo: true

# Export module for AMD
module.exports = docpadConfig
