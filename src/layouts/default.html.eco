<!DOCTYPE html>
<html class="no-js">
<head>
    <title><%= @getPreparedTitle() %></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<%= @getPreparedDescription() %>">
    <meta name="keywords" content="<%= @getPreparedKeywords() %>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script>document.documentElement.className=document.documentElement.className.replace('no-js','js');</script>
    <%- @getBlock("meta").toHTML() %>
    <!--[if gt IE 8]> <!-- -->
    <%- @getBlock("styles").add(['/css/main.css']).toHTML() %>
    <!-- <![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="/css/main-no-mqs.css">
    <![endif]-->
</head>
<body>
    <section class="container container--primary">
        <header class="header header--primary" role="banner">
            <h1 class="header__title">
                <a href="/"><span class="header__title__wrap"><%=@site.title%></span></a>
                <span class="header__strapline"><%=@site.strapline%></span>
            </h1>
            <nav class="nav nav--primary" role="navigation">
                <ul class="nav__items">
                    <li class="nav__item">
                        <a class="nav__item__label" href="/blog">
                            Bl<span class="icon icon--bubble nav__icon"><span class="icon-text">o</span></span>g
                        </a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__item__label" href="/work">
                            W<span class="icon icon--cog nav__icon"><span class="icon-text">o</span></span>rk
                        </a>
                    </li>
                    <li class="nav__item"><a class="nav__item__label" href="/about"><span class="alt">&amp;</span>nd Me</a></li>
                </ul>
            </nav>
        </header>

        <main role="main">
            <article class="content content--primary">
                <%- @content %>
            </article>
        </main>

        <section class="content content--alt">
            <aside role="complementary">

                <section class="list list-liks">
                    <h1>More related</h1>
                    <ul>
                        <% for document in @document.relatedDocuments: %>
                            <a href="<%= document.url %>"><%= document.title %></a><br/>
                        <% end %>
                    <% for post in @getCollection("posts").toJSON(): %>
                        <li><a href="<%= post.url %>"><%= post.title %></a></li>
                    <% end %>
                    </ul>
                <section>
                
                <section>
                    <h1>Photos</h1>
                    <ul>
                    <% for photo in @getCollection("photos").toJSON(): %>
                        <li><a href="<%= photo.url %>"><%= photo.title %></a></li>
                    <% end %>
                    </ul>
                </section>
                
                <section>
                    <h1>Projects</h1>
                    <ul>
                    <% for project in @getCollection("projects").toJSON(): %>
                        <li><a href="<%= project.url %>"><%= project.title %></a></li>
                    <% end %>
                    </ul>
                </section>
            </aside>
        </section>
        
        <footer class="footer--primary" role="contentinfo">
            <small>
                <span class="footer__wrap">&copy; <%=@getYear()%> Justin Perry</span>
            </small>
            <nav class="footer__items">
                <span class="footer__wrap">
                    <a href="/blog" class="footer__item">Blog</a>
                    <a href="/work" class="footer__item">Work</a>
                    <a href="/about" class="footer__item">About</a>
                    <a href="http://www.github.com/ourmaninamsterdam" class="footer__item">Github</a>
                </span>
            </nav>
        </footer>
    </section>
    <%- @getBlock("scripts").add(['/js/main.js'].concat(@document.scripts or [])).toHTML() %>
</body>
</html>
