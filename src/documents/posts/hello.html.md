---
title: Building a blog with Docpad
tags:
	- web
---

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, ex, odio, aut, voluptate tempora delectus nostrum officiis vitae sed quo quam pariatur consequatur temporibus culpa iure quidem iste odit! *Eaque. Doloribus, ipsa vero architecto harum iure quaerat quia odio non maxime* possimus repellat laborum ducimus sint quae beatae laudantium omnis rem provident aliquam facere. Autem necessitatibus fugiat voluptatum tempora vitae!

**Consectetur, excepturi, mollitia unde odit distinctio** quas quidem itaque vel optio odio ullam placeat dolore! Dolore, necessitatibus inventore nostrum nihil non laborum eos quam eius tempora ab. Recusandae, quo sit.

![Dummy image](http://placekitten.com/850/600)

Delectus libero id non. Ipsam, maiores, ad, et `aperiam iusto dolores` vero molestiae dolor tempore quibusdam reprehenderit placeat laboriosam aliquid corporis odio ea omnis! Veniam, accusamus inventore enim magni ex!
Vero sunt aperiam aspernatur explicabo ea tenetur deleniti? Vero, maxime, officia eius ratione voluptate consectetur alias placeat laborum debitis ex quaerat cupiditate veniam unde ea culpa harum nobis molestiae ut!

<img src="http://placekitten.com/300/250" class="media media--left"> Dicta, tempore cumque deserunt officia ratione debitis tempora nemo repellendus porro architecto distinctio voluptatibus praesentium quisquam asperiores molestias fugit quia! Similique, ipsam impedit a provident minima. Amet, doloremque nisi culpa.
Quam, nisi, deleniti nulla natus pariatur harum non deserunt voluptate impedit esse inventore magni dolorum ipsum error dignissimos! Ut culpa consectetur sed quae deleniti omnis corporis voluptatem est soluta ab!

<img src="http://placekitten.com/300/250" class="media media--right"> Dicta, tempore cumque deserunt officia ratione debitis tempora nemo repellendus porro architecto distinctio voluptatibus praesentium quisquam asperiores molestias fugit quia! Similique, ipsam impedit a provident minima. Amet, doloremque nisi culpa.
Quam, nisi, deleniti nulla natus pariatur harum non deserunt voluptate impedit esse inventore magni dolorum ipsum error dignissimos! Ut culpa consectetur sed quae deleniti omnis corporis voluptatem est soluta ab!

<img src="http://placekitten.com/400/300" class="media media--center"> 
Quam, nisi, deleniti nulla natus pariatur harum non deserunt voluptate impedit esse inventore magni dolorum ipsum error dignissimos! Ut culpa consectetur sed quae deleniti omnis corporis voluptatem est soluta ab!

> This is blockquote taken from the main body of text. And put on two lines.

