# A blogging platform by Justin Perry


This is my own custom blog platform, built using static site generator, [Docpad](http://docpad.org).

### Features
* Tags
* Drafts
* Related Docs
* Custom code snippets
* CodePen embeds
* Custom page scripts
* Custom templates

Each post/page/project is written in markdown (with HTML and a sprinkling of eco) then processed using Docpad to output a static site.

## Languages
* HTML5
* CSS3 and Sass
* JavaScript (and some CoffeeScript)
* eco
* Markdown

### CSS 
Modular using Sass partials. Follows a BEM approach.

## JavaScript
...

## License
Copyright 2013 All rights reserved.